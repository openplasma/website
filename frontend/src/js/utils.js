export function mergeObjectsNoOverride(mainObj, secondaryObj) {
  var mergedObj = JSON.parse(JSON.stringify(mainObj));

  // Add every item from secondaryObj which is NOT in mainObj to mergedObj
  for (const [key, value] of Object.entries(secondaryObj)) {
    if (typeof value === "object") {
      if (key in mainObj) {
        mergedObj[key] = mergeObjectsNoOverride(mergedObj[key], value);
      } else {
        mergedObj[key] = value;
      }
    } else {
      if (!(key in mainObj)) {
        mergedObj[key] = value;
      }
    }
  }
  return mergedObj;
}
