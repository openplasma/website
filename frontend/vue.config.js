module.exports = {
  css: {
    loaderOptions: {
      scss: {
        additionalData: `@import "@/scss/_colors.scss";
                         @import "@/scss/_sizes.scss";`,
      },
    },
  },
};
