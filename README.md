# OpenPlasma-website (Work In Progress, incredibly WIP)

Inspired by the fantastic success of lxcat.net comes OpenPlasma.net which will feature many different plasma physics based applications.

All tools featured on the website will be completely free and open-source and available on the OpenPlasma repository.

# Applications

## Database

Features of the database will be:

- Reaction rates and scattering cross-sections can be stored/retrieved in functional, tabular, or constant form
- Each reaction rate and cross-section can be visualized within the application
- Each entry within the database will have a unique ID which will never change to increase reproducibility within the scientific community
- Physics changing corrections to database entries will produce new entries with their own unique ID and users will be notified when accessing the incorrect database entry.
- The entire list of selected reactions will produce a single unique ID to easily share data with peers and within papers.
- Each entry within the database will be tied to a peer-reviewed publication.
- Automatic generation of a bibtex file and latex file displaying all reactions within a table including citations to make the life of scientists just a little bit easier
- Species for each entry in the database are specified by their charged, electronic, vibrational, and rotational state (if known).
- Filters can be added when querying the database to e.g. only retrieve data obtained from experiments or from theoretical calculations
